"""Small function library for grammatical correctness."""

from typing import List, Set, Tuple, Union


def plural(n: str, p: str = "s", s: str = "", w: str = "") -> str:
    # Given grammar and a number, return the appropriate singular or plural form
    return w + {True: p, False: s}[n != 1]


def get_a(word: str, include: bool = False) -> str:
    word: str = word.lstrip()
    loword: str = word.lower()
    b: str = (" " + word) if include else ""
    if loword[0] in "aeiou" or (loword[0] == "y" and loword[1] not in "aeiou"):
        return "an" + b
    else:
        return "a" + b


def sequence_words(words: Union[List[str], Set[str], Tuple[str]]) -> str:
    if not words:
        return ""
    words: List[str] = [str(s) for s in words]
    last: str = words.pop(-1)
    most: str = ", ".join(words)
    return (", and " if len(words) > 1 else " and ").join([most, last]) if most else last
