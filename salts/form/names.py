"""Expose user information such as Username to the program.

This information is neither saved nor sent anywhere, and is used only for more
    personalized user interface.
"""

import getpass


username = getpass.getuser()
