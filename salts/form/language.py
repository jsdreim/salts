"""Module providing mostly strings to be formatted and displayed."""

yes = ["yes", "y", "yeah", "yea", "ya", "ye", "yup", "aye", "si", "ja", "oui"]
no = ["no", "n", "nah", "na", "nu", "nope", "nay", "nein", "non"]

yn = lambda b: "yes" if b else "no"

greet_base = "Good {time}, {name}."

prompts_first = [
    "How can I help you?",
    "Something I can help with?",
    "What can I do for you?",
    "What do you need?",
]
prompts_subsequent = [
    "Can I do anything else for you?",
    "Do you need anything else?",
    "Is there something else I can help you with?",
    "What else do you need?",
]
