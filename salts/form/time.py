"""Module for the parsing of time into standard forms."""

from datetime import datetime, time

from dateparser import parse

from ..etc.config import cfg


def f_date(dt: datetime):
    return dt.strftime("%A, %e %B, %Y")


def f_time(dt: datetime):
    return dt.strftime("%l:%M %p")


def find_time(when: str) -> datetime:
    return parse(when)


def now() -> datetime:
    """Return the current time, in the local timezone."""
    dt = datetime.utcnow().astimezone()
    dt += dt.utcoffset()
    return dt


def time_of_day(when: datetime = None) -> str:
    """Return a string of what "part" of the day it is."""
    when: datetime = when or now()
    when: time = when.timetz()
    current = "day"
    for t, word in cfg.get("day_parts", {}).items():
        if when.hour >= t:
            current = word
        else:
            break
    return current
