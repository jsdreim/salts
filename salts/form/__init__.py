"""Package providing functionality to format text. for display to the user."""

from . import language, time, names


def greeting():
    return language.greet_base.format(
        name=names.username.capitalize(), time=time.time_of_day()
    )


def indented(text: str, indent=4, split="\n", skip_empty_lines=True):
    return split.join(
        [
            " " * indent + line
            for line in text.split(split)
            if line or not skip_empty_lines
        ]
    )
