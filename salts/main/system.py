"""System commands module. Defines commands that interact with the system
    itself.
"""

from .core import CmdCore
from ..etc.config import cfg
from ..etc.execution import bash, command_exists
from ..form.grammar import sequence_words


class CmdSystem(CmdCore):
    def do_install(self, line, **_):
        """Install some new software from the Internet onto your System.
        """
        pkgs = sequence_words(line.split())
        if line:
            print("Trying to install {} from the Internet. You may be asked for "
                 "your password.".format(pkgs))
        else:
            print("You haven't specified any programs to install.")
            return

        cmd = cfg.get("commands/install")
        if cmd and command_exists(cmd):
            install = bash("sudo", *cmd.format(line).split(), wait=True)
            if install == 0:
                print("Installed {}.".format(pkgs))
            else:
                print("Could not install everything.")
        else:
            print("Sorry, I cannot seem to find an install command.")


    def do_update(self, **_):
        """Upgrade your operating system. I will try a few of the most common
            update commands to see if one works.
        """
        print("Updating system...You may be asked for your password.")
        for cmd in cfg.get("commands/update", []):
            if command_exists(cmd):
                result = bash("sudo", *cmd.split(), wait=True)
                if result == 0:
                    print("System updated.")
                else:
                    print("I found a command, but it looks like it failed.")
                return

        print("Sorry, I cannot seem to find an update command.")
