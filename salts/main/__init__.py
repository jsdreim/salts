"""Primary functional package.
"""

from .shortcuts import CmdShortcut
from .system import CmdSystem
from .weather import CmdWeather, weather_init, print_weather


class CmdMain(CmdShortcut, CmdSystem, CmdWeather):
    """This class is the entry point of the program. It greets the user and
        implements the top level commands.
    """
