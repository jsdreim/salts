"""Module defining the base level of the SALTS interpreter."""

from getopt import GetoptError

from ..etc.cmdi import Cmd, handle_opts
from ..etc.rand import pick
from ..form import indented, language
from ..form.grammar import get_a


class CmdCore(Cmd):
    count = 0
    doc_header = "Commands I can help you with (type 'help ______'):"
    misc_header = "Other things I can help you with:"
    undoc_header = "Commands you can use, but which I can't help with:"
    nohelp = "Sorry, I don't have any information about '%s'."

    @property
    def prompt(self):
        self.count += 1
        if self.count < 2:
            pool = language.prompts_first
        else:
            pool = language.prompts_subsequent
        return "\n" + pick(pool) + "\n> "

    def error(self, e, **_):
        print(
            "Sorry, I couldn't figure out what you meant. I got {} '{}' which"
            " says this:\n{}\n".format(
                get_a(type(e).__name__), type(e).__name__, indented(str(e), 4)
            )
        )

    def default(self, line: str, **_):
        if line == "EOF":
            print("exit")
            return self.do_exit()
        else:
            print(
                "Sorry, I don't know what to do for the command '{}'. "
                "Remember that you can type 'help' or a question mark "
                "(?) if you need help.".format(line)
            )
            return False

    def do_exit(self, **_):
        """Close the program.
        """
        exit(0)

    def onecmd(self, line):
        """Interpret the argument as though it had been typed in response
        to the prompt.

        This may be overridden, but should not normally need to be;
        see the precmd() and postcmd() methods for useful execution hooks.
        The return value is a flag indicating whether interpretation of
        commands by the interpreter should stop.

        """
        cmd, arg, args, line = self.parseline(line)
        if not line:
            return self.emptyline()
        if cmd is None:
            return self.default(line)
        self.lastcmd = line
        if line == "EOF":
            self.lastcmd = ""
        if cmd == "":
            return self.default(line)
        else:
            try:
                func = getattr(self, "do_" + cmd)
            except AttributeError:
                return self.default(line)

            try:
                opts, args = handle_opts(func, args)
            except (GetoptError, TypeError) as e:
                return self.error(e, func=func, line=line, args=args)
            else:
                try:
                    return func(line=arg, args=args, **opts)
                except Exception as e:
                    ename = type(e).__name__
                    print(
                        "Sorry, I ran into a problem and could not run that "
                        "command. The error is {} '{}' and it says this:\n{}".format(
                            get_a(ename), ename, indented(str(e), 4)
                        )
                    )
