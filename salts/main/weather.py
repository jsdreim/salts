"""Weather module. Defines commands and functions for retrieving and displaying
    data about the Weather from OpenWeatherMaps.
"""

from datetime import datetime as dt
from typing import Callable, List, Optional

from pyowm import OWM
from pyowm.exceptions.api_response_error import NotFoundError
from pyowm.weatherapi25 import (
    forecast,
    forecaster,
    location,
    observation,
    owm25,
    weather,
)

from .core import CmdCore
from ..etc.config import cfg
from ..form.grammar import sequence_words
from ..form.time import f_date, f_time, find_time, now


enabled: bool = False
here: location.Location
owm: owm25.OWM25 = OWM(use_ssl=True)


def weather_init():
    global enabled
    owm.set_API_key(cfg.get("weather/api"))
    enabled = True


def conditions_list(day: forecaster.Forecaster) -> List[str]:
    conditions: List[str] = []
    if day.will_have_rain():
        conditions.append("rain")
    if day.will_have_snow():
        conditions.append("snow")
    if day.will_have_storm():
        conditions.append("a storm")
    if day.will_have_tornado():
        conditions.append("a tornado")
    if day.will_have_hurricane():
        conditions.append("a hurricane")
    return conditions


def weather_now() -> Optional[observation.Observation]:
    if not enabled:
        return None
    return owm.weather_at_zip_code(*cfg.get("weather/zip", ("", "")))


def weather_future(where: location.Location) -> Optional[forecaster.Forecaster]:
    if not enabled:
        return None
    return owm.three_hours_forecast_at_coords(where.get_lat(), where.get_lon())


def weather_where(
    condition: Callable[[weather.Weather], bool], where: location.Location
) -> Optional[forecaster.Forecaster]:
    if not enabled:
        return None
    fcaster: forecaster.Forecaster = weather_future(where)
    if fcaster is not None:
        fcast: forecast.Forecast = fcaster.get_forecast()

        # Build a new Forecaster comprised of all the Weathers for today.
        fc_today: forecaster.Forecaster = forecaster.Forecaster(
            forecast.Forecast(
                fcast.get_interval(),
                fcast.get_reception_time(),
                fcast.get_location(),
                [w for w in fcast if condition(w)],
            )
        )
        fc_today.get_forecast().actualize()
        return fc_today


def print_weather():
    tn: dt = now()
    print("It is currently {} on {}.".format(f_time(tn), f_date(tn)).replace("  ", " "))

    ob: observation.Observation = weather_now()
    if ob:
        global here
        here = ob.get_location()
        data: weather.Weather = ob.get_weather()
        sun0: dt = dt.fromtimestamp(data.get_sunrise_time())
        sun1: dt = dt.fromtimestamp(data.get_sunset_time())
        print(
            "Sunrise today is at {}, and Sunset is at {}.".format(
                f_time(sun0), f_time(sun1)
            ).replace("  ", " ")
        )
        stat: str = data.get_detailed_status()
        print("The current condition is {}.".format(stat))

        is_today: Callable[[weather.Weather], bool] = (
            lambda weather_: weather_.get_reference_time("date").astimezone().date()
            == tn.date()
        )
        today: forecaster.Forecaster = weather_where(is_today, here)

        unit: str = cfg.get("weather/degrees", "fahrenheit")
        print(
            "Right now it is {}°, and the rest of the day should be"
            " between {}° and {}°.".format(
                int(data.get_temperature(unit).get("temp", 0)),
                int(today.most_cold().get_temperature(unit).get("temp_min", 0)),
                int(today.most_hot().get_temperature(unit).get("temp_max", 0)),
            )
        )

        conditions: List[str] = conditions_list(today)
        if conditions:
            print("There may be {} later today.".format(sequence_words(conditions)))


class CmdWeather(CmdCore):
    def do_forecast(self, line: str, **_):
        """Display Weather information from the future.

        Provide a relative time in the future, such as "noon tomorrow" or "6pm
            on the 10th", and SALTS will try to retrieve a Weather Forecast for
            that time.

        Using the free tier of OpenWeatherMaps API, can only look 5 days ahead.
        """
        tn: dt = now()
        if line:
            when: dt = find_time(line)
            if when is not None:
                when = when.astimezone()
            else:
                print("Sorry, I don't understand when you mean by '{}'.".format(line))
                return

            if when < tn:
                print("Sorry, I can't get historical data.")
                return

            fc: forecaster.Forecaster = weather_future(here)
            if not fc:
                print("Sorry, I can't access the API.")
                return

            try:
                w: weather.Weather = fc.get_weather_at(when)
            except NotFoundError:
                days = when - tn
                print(
                    "Sorry, I don't have any data for {} on {}.".format(
                        f_time(when), f_date(when)
                    ).replace("  ", " ")
                )
                if 5 <= days.days:
                    # User may be looking too far forward.
                    # TODO: Check Subscription tier before mentioning this.
                    print(
                        "The Free Subscription of OpenWeatherMaps will only let"
                        " us look up to five days ahead."
                    )

            else:
                unit: str = cfg.get("weather/degrees", "fahrenheit")
                when_data: dt = w.get_reference_time("date")
                print(
                    "Around {} on {}, expect {} and a temperature of {}°.".format(
                        f_time(when_data),
                        f_date(when_data),
                        w.get_detailed_status(),
                        int(w.get_temperature(unit).get("temp", 0)),
                    ).replace("  ", " ")
                )

                if when.date() != tn.date():
                    same_day: Callable[[weather.Weather], bool] = (
                        lambda weather_: weather_.get_reference_time("date")
                        .astimezone()
                        .date()
                        == when.date()
                    )
                    fcast_daily: forecaster.Forecaster = weather_where(same_day, here)
                    print(
                        "The temperature for the day should be between"
                        " {}° and {}°.".format(
                            int(
                                fcast_daily.most_cold()
                                .get_temperature(unit)
                                .get("temp_min", 0)
                            ),
                            int(
                                fcast_daily.most_hot()
                                .get_temperature(unit)
                                .get("temp_max", 0)
                            ),
                        )
                    )

                    conditions: List[str] = conditions_list(fcast_daily)
                    if conditions:
                        print(
                            "There may be {} throughout the day.".format(
                                sequence_words(conditions)
                            )
                        )
                else:
                    print("(This is today.)")
        else:
            print_weather()
