"""Shortcut commands module. Defines commands that call out to other programs
    installed on the system.
"""

from typing import Callable, List
from urllib.parse import quote

from .core import CmdCore
from ..etc.config import cfg
from ..etc.execution import xdg_email, xdg_open
from ..form.grammar import sequence_words


url_google: Callable[
    [str], str
] = lambda q: "https://www.google.com/search?q={}".format(quote(q))


class CmdShortcut(CmdCore):
    def do_email(self, line: str, **_):
        """Open your Mail client and prepare to send a Message."""
        print("Opening your Mail Client...")
        xdg_email(line)

    def do_internet(self, args: List[str], **_):
        """Open your Browser and go online."""
        if args:
            print("Opening your Web Browser to {}...".format(sequence_words(args)))
            shortcuts: dict = cfg.get("internet/shortcuts", {})
            for site in args:
                low: str = site.lower()
                if low in shortcuts:
                    xdg_open(shortcuts[low])
                else:
                    xdg_open("https://" + site)
        else:
            print("Opening your Web Browser...")
            xdg_open(cfg.get("internet/homepage", "https://www.google.com"))

    def do_google(self, line: str, **_):
        """Open Google in your Browser to search the Internet for something."""
        if line:
            print("Opening your Web Browser to search Google...")
            xdg_open(url_google(line))
        else:
            print("Opening the Google Homepage...")
            xdg_open("https://www.google.com")

    do_search = do_google

    def do_open(self, line: str, **_):
        """Open a link or a file in whatever program seems most appropriate.

        I may get this wrong, but I will do my best.
        """
        if line:
            print("Opening:", line)
            xdg_open(line)
        else:
            print("Nothing provided to open.")

    def do_reddit(self, args: List[str], **_):
        """Open Reddit. If you provide a name, I will open that specific page of
            Reddit, instead of the homepage.
        """
        if args:
            print(
                "Opening your Web Browser to {}...".format(
                    sequence_words(["/r/" + sub for sub in args])
                )
            )
            for sub in args:
                xdg_open("https://www.reddit.com/r/" + sub)
        else:
            print("Opening your Web Browser to the Reddit Homepage...")
            xdg_open("https://www.reddit.com")
