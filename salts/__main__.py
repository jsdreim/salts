"""SALTS, the Senior-Assistive Linux Tool Suite. General purpose system utility.

Built on a modified version of the `cmd` module from the Python StdLib, found
    in `etc/cmdi`.
"""

__title__ = "salts"
__author__ = "JS Dreim"
__license__ = "GPLv3"
__copyright__ = "Copyright 2019 JS Dreim"


from getopt import getopt
from pathlib import Path
from pkg_resources import Requirement, ResolutionError, resource_filename
from readline import read_history_file, set_history_length, write_history_file
from sys import argv

from ._version import __version__
from .etc import cfg, maint, saltio
from .etc.execution import killall
from .form import greeting, indented
from .form.grammar import get_a
from .main import CmdMain, print_weather, weather_init


try:
    cfg_default = resource_filename(Requirement.parse(__title__), "salts/config.yml")
except ResolutionError:
    cfg_default = "salts/config.yml"

history_path_override = None
runner = None

executable = Path(argv.pop(0))


# Break down and parse all options.
opts, args = getopt(
    argv, "c:h:o:", ["config=", "history=", "no-default-config", "no-update", "option="]
)
flags = [k for k, v in opts if not v]

# Load the default config, unless the user said not to.
if "--no-default-config" not in flags:
    cfg.load(cfg_default)
    cfg.load()

# Only try to update if the user has not forbidden it.
try_update = "--no-update" not in flags

# Loop through the options properly now.
for k, v in opts:
    if k in ("-c", "--config"):
        # Load any other configuration files the user has specified, in order.
        cfg.load(v)
    elif k in ("-h", "--history"):
        # Specify a non-default path for the Command history.
        history_path_override = v
    elif k in ("-o", "--option"):
        # Update the Config profile with specific override values provided
        #   manually. Advanced users only.
        for k1, v1 in [kv.split("=") for kv in v.split(",") if "=" in kv]:
            cfg.set(k1, v1)


history_file = Path(
    history_path_override or cfg.get("history/file", ".history")
).resolve()
weather_init()


def start():
    global runner
    if try_update:
        needed, ver = maint.check(__version__)
        if needed and saltio.ask_bool(
            "The current '{2}' version of SALTS is Version {0}, but you are "
            "running Version {1}. Would you like to change to Version {0}?".format(
                ver, __version__, maint.branch
            ),
            False,
        ):
            maint.upgrade()
        elif needed is False:
            print("Skipping update.")
    print(
        "Starting up SALTS version [{}]. Type 'help' or a question mark (?) "
        "for assistance.\n\n".format(__version__)
    )

    print(greeting())
    try:
        print_weather()
    except:
        pass
    runner = CmdMain()
    runner.cmdloop()


if __name__ == "__main__":
    try:
        try:
            try:
                read_history_file(str(history_file))
            except FileNotFoundError:
                pass
            set_history_length(cfg.get("history/length", 30))
            start()
        except KeyboardInterrupt:
            print("exit")
        except maint.Restart as e:
            raise e
        except Exception as e:
            ename = type(e).__name__
            print(
                "Sorry, I ran into a problem and need to close early. The error "
                "is {} '{}' and it says this:\n{}".format(
                    get_a(ename), ename, indented(str(e), 4)
                )
            )
        finally:
            print("Closing down. I'll be here when you need me.")
            write_history_file(str(history_file))
            killall()
    except maint.Restart:
        print("Closing SALTS. You can just reopen it right away.")
