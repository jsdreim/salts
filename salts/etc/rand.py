"""Randomness module. Implements a set of functions for variation."""

import random


def pick(options, quantity=1):
    if quantity > 1:
        return random.sample(options, quantity)
    else:
        return random.choice(options)
