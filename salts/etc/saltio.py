"""Module for User Interaction."""

from typing import List, Optional

from ..form.grammar import sequence_words
from ..form.language import no, yes, yn


def ask(question: str, allowed: List[str] = None, default: str = None) -> Optional[str]:
    allowed = allowed or []
    print(question)

    helpline = (
        "The possible answers are {}.".format(
            sequence_words([repr(x) for x in allowed])
        )
        if allowed
        else "You can answer with anything you want."
    )
    if default:
        helpline += "\n(If I don't understand your answer, I'll assume '{}'.)".format(
            default
        )
    print(helpline)

    while True:
        ans = input("Type your answer, or 'cancel' to go back: ")
        if ans.lower() == "cancel":
            print("Cancelled.")
            return None
        elif allowed:
            if ans in allowed:
                return ans
            else:
                if default:
                    return default
                else:
                    print("Sorry, I can't understand that. Could you try again?")
                    continue
        else:
            return ans or default or ""


def ask_bool(question: str, default: bool = False) -> bool:
    reply = input(
        "{}\n"
        "(If I don't understand your answer, I'll assume '{}'.)\n"
        "Type your answer: ".format(question, yn(default))
    ).lower()
    if reply in yes:
        return True
    elif reply in no:
        return False
    else:
        return default
