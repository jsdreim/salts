"""Module for self-maintenance; Upgrading and installing SALT."""

from pkg_resources import parse_version

import requests

from .._version import __version__
from .execution import bash, command_exists


branch = "master"
vloc = f"https://gitlab.com/jsdreim/salts/raw/{branch}/salts/_version.py"


class Restart(Exception):
    pass


def check(current: str = __version__):
    """Fetch the file showing the latest version from the repository."""
    print("Checking for an update to SALTS...")
    req = requests.get(vloc)
    if req:
        file = req.content.decode("utf-8").split("\n")
        ver = ""
        for line in file:
            if line.startswith("__version__ = "):
                ver = eval(line.split("=")[1])
        if ver:
            if parse_version(ver) <= parse_version(current):
                print("You have the latest version of SALTS.")
                return None, current
            else:
                print("Found a newer version.")
                return True, ver
        else:
            print("Sorry, I can't tell what version this is.")
    else:
        print("I can't find the repository.")
        return False, current


def upgrade():
    if command_exists("git"):
        if bash("git", "pull", wait=True) == 0:
            print(
                "Successfully updated SALTS; You will need to exit and reopen"
                " before any changes take effect."
            )
            # raise Restart
        else:
            print("Could not update SALTS.")
    else:
        print("Cannot find the Update command.")
