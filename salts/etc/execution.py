"""Execution module. Handles running code on the system itself."""

from subprocess import CompletedProcess, DEVNULL, PIPE, Popen, run
import sys
from typing import IO, List, Union

from colorama import Fore

from .config import cfg


procs: List[Popen] = []


def bash(
    *command: str,
    quiet: bool = False,
    wait: bool = False,
    stdout: Union[None, int, IO] = PIPE,
    stderr: Union[None, int, IO] = PIPE,
    **kw
) -> Union[int, Popen]:
    if not command:
        raise ValueError("Cannot execute empty command.")
    proc = Popen(command, stdout=stdout, stderr=stderr, **kw)

    if wait:
        # Allow the Process to run in full, before resuming SALTS. The Process
        #   return status code will be Returned.
        try:
            if not quiet:
                # While waiting for the Process to finish, forward its Output.
                print(Fore.WHITE + ">>>", *command)
                if proc.stdout is not None:
                    for line in proc.stdout:
                        print("...", line.decode(), end="")

            r = proc.wait()
        except KeyboardInterrupt:
            print(Fore.RED + "Process Cancelled.")
            print(Fore.RESET)
            return 4
        else:
            print(Fore.RESET, end="")
            return r
    else:
        # Make a note of the Process, and then return it.
        procs.append(proc)
        return proc


def command_exists(cmd: str) -> bool:
    cmd: str = cmd.split()[0]
    found: CompletedProcess = run(
        'command -v "{}"'.format(cmd), stdout=DEVNULL, stderr=DEVNULL, shell=True
    )
    return found.returncode == 0


def echo(*lines: str, pre: str = ""):
    run(
        'echo -e "{}"'.format("\n".join([pre + line for line in lines])),
        stdout=sys.stdout,
        stderr=sys.stderr,
        shell=True,
    )


def killall():
    # Kill all known child Processes.
    for p in procs:
        p.kill()


def terminal(line: str, hold: bool = False) -> Popen:
    print("Opening in a new Terminal Window.")
    return bash(cfg.get("terminal", "xterm"), "--hold" if hold else "", "-e", line)


def xdg_email(dest: str = "") -> Popen:
    return bash("xdg-email", dest) if dest else bash("xdg-email")


def xdg_open(dest: str) -> Popen:
    return bash("xdg-open", dest)
