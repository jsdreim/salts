"""SALTS Testing Module. Built for use with PyTest.

Test Configuration reader and handler.
"""

from salts.etc import config


conf = config.ConfigReader()


def test_config_empty():
    conf.clear()
    assert conf.get("asdf") is None, "Bad config field does not return None by Method."
    assert conf.asdf is None, "Bad config field does not return None by Attribute."

def test_config_dict():
    conf.cfg_dict["asdf"] = 1337
    assert conf.get("asdf") == 1337, "Cannot access Config by Method."
    assert conf.asdf == 1337, "Cannot access Config by Attribute."

def test_config_attr():
    conf.asdf = 420
    assert conf.get("asdf") == 1337, "Setting Config Attribute changes Dict."
    assert conf.asdf == 420, "Setting Config Attribute does not suppress Dict."
