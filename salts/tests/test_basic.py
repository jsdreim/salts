"""SALTS Testing Module. Built for use with PyTest.

Sanity check and test basic functions and utilities.
"""

from subprocess import Popen
from sys import stderr, stdout

from salts.etc.execution import bash


def test_math():
    # Perform sanity checks. If any of these go wrong, something is doubleplusungood.
    assert 1 == 1, "Math is broken."
    assert 2 + 2 != 5, "Oceania has always been at war with Eastasia."
    assert 4 * 5 == 20, "Math is broken."
    assert 2 ** 4 == 16, "Math is broken."


def test_shell():
    # Test the Bash Module. Make sure everything works correctly.
    proc = bash("echo", "Testing for Shell access...", stdout=stdout, stderr=stderr)
    assert isinstance(proc, Popen), "Non-waited Process execution returns bad type."
    ret = proc.wait(5)
    assert ret == 0, "Process failed to Echo, returned status '{}'.".format(ret)
    ret = bash(
        "echo", "Testing for Shell access...", wait=True, stdout=stdout, stderr=stderr
    )
    assert ret == 0, "Waited Process execution returned status '{}'.".format(ret)
