# SALTS

SALTS, the Senior-Assistive Linux Tool Suite. General purpose utility  
built for my grandmother. Repository exists primarily for convenience  
to update.

Developed with Arch Linux in mind.

While I am developing this solely for the needs of one user, I am open  
to contributions, but an unchanging user experience is **priority #1.**

---

SALTS can be thought of essentially as an extremely simplified shell,  
with very limited capabilities that each do only a single thing. It is,  
first and foremost, a set of "shortcuts" wrapped in an interface that  
needs very little technical understanding to use.

### Commands

Keyword | Function/Effect | Example
---:|:---:|:---
`exit` | Close the program and return to the outer shell, if any. Used over SSH, this might close the entire connection. | `exit`: Closes SALTS.
`help` / `?` | Display help, either by listing all commands or by printing details of a specified command. | `help`: Lists all Commands; `help open`: Show Help Text for `open`.
`update` | Automatically perform system updates. Currently, merely by way of brute force guessing commands until one works. May require Passphrase input. | `update`: Performs System updates.
`install` | Install a program from the Internet. With the default Configuration, this will use `pacman -S`. May require Passphrase input. | `install neofetch`: Finds and installs NeoFetch from the OS Repositories.
`email` | Open your Mail Client to send an Email. Uses `xdg-email`. | `email`: Opens the Mail Client.
`open` | Provided a File Path or a URL, open it in the appropriate Program. Uses `xdg-open`. | `open /etc/hosts`: Opens the Hosts file in your default Text Editor; `open google.com`: Opens *google.com* in your Browser.
`internet` | If no Argument is specified, open the Configured SALTS homepage. If an Argument is specified which is a Configured Shortcut, open the URL associated with that Shortcut. Otherwise, assume that the Argument is a domain name, and try to open it directly. | `internet`: Opens the Homepage; `internet netflix`: With default Config, opens *netflix.com*; `internet asdfqwert.org`: Opens *asdfqwert.org*.
`google` / `search` | If no Argument is specified, open the Google homepage. If further text is given, open a Google search for the text. | `search how to remove Chrome?`: Opens up a Google Search page for *"how to remove Chrome?"*.
`reddit` | If no Argument is specified, open the homepage of Reddit. If arguments are provided, open the Reddit pages of ALL arguments. | `reddit cats askreddit`: Opens *reddit.com/r/Cats* **and** *reddit.com/r/AskReddit*
`forecast` | Display Weather information from the future. A date and/or time may be provided, in a wide variety of human-written formats, such as "tomorrow", or "at noon", or "on the 23rd". With a free-tier OpenWeatherMaps API key, can only look five days ahead, and at three-hour intervals. | `forecast 3pm tomorrow`: Displays information about the forecasted Weather at 3pm, local time, tomorrow.

---

*Documentation last updated for Version `1.0.0b2`*
